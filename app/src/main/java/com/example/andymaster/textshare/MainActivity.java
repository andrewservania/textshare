package com.example.andymaster.textshare;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.googlecode.tesseract.android.TessBaseAPI;

public class MainActivity extends ActionBarActivity {


    private Button captureButton;
    private Button shareButton;
    private Button copyButton;




    private Uri capturedImageURI;
    private static final int PHOTO_REQUEST= 1000;
    private static final String DATA_PATH = "/storage/sdcard1/tesslanguagefiles";
    private static final String lang = "eng";

    private Camera mCamera;
    private CameraPreview mPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create an instance of Camera
        mCamera = getCameraInstance();

        // Get the picture button.
        captureButton = (Button) findViewById(R.id.capture_button);
        shareButton = (Button) findViewById(R.id.share_button);
        copyButton = (Button) findViewById(R.id.copy_button);

        // Get and set camera video feed
        mPreview = new CameraPreview(this, mCamera);
        RelativeLayout preview = (RelativeLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            //TODO: Auto-generated method stub
            super.onActivityResult(requestCode, resultCode, data);


    }

    public void onCaptureButtonClick(View view) {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.putExtra(MediaStore.EXTRA_OUTPUT,capturedImageURI);
//        startActivityForResult(intent,PHOTO_REQUEST);

        //File imageFile = new File("/storage/sdcard1/DCIM/100ANDRO/test1.jpg");
        //Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        mCamera.takePicture(null, null, mPicture);

    }

    //In order to retrieve a picture, use the Camera.takePicture() method.
    // This method takes three parameters which receive data from the camera.
    // In order to receive data in a JPEG format, you must implement an Camera.
    // PictureCallback interface to receive the image data and write it to a file.
    // The following code shows a basic implementation of the Camera.PictureCallback
    // interface to save an image received from the camera.
    public Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            String recognizedCharacters = getRecognizedCharacters(rawPictureDataToBitmap(data, camera));
            EditText recognizedText = (EditText) findViewById(R.id.recognizedTextField);
            recognizedText.setText(recognizedCharacters);
            restartCamera();


        }
    };

    private void restartCamera() {
        stopCamera();
        startCamera();
    }

    public Bitmap rawPictureDataToBitmap(byte[] data, Camera camera) {

        Bitmap bmp = BitmapFactory.decodeByteArray(data,0,data.length);
        Bitmap bitmapImage = bmp.copy(Bitmap.Config.ARGB_8888,true);
        return bitmapImage;
    }

    public void onShareButtonClick(View view) {

    }

    public void onCopyButtonClick(View view) {

    }

    public String getRecognizedCharacters(Bitmap capturedImage) {
        try {
            // This is a class for reading and writing Exif tags in a JPEG file.
            // Useful for rotating pictures in the future
            // Remember, tessaract can only detect characters when the image is rotated properly(letters should be readable angle)
//            String _path = "/storage/sdcard1/DCIM/100ANDRO/test1.jpg";
//            ExifInterface e = new ExifInterface();
//
//            int ex = e.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_NORMAL);
//
            int rotate = 0;
//
//            switch (ex) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotate = 90;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotate = 180;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotate = 270;
//                    break;
//            }


            if (rotate != 0) {
                int imageWidth = capturedImage.getWidth(); // Gets width of the bitmap
                int imageHeight = capturedImage.getHeight(); // Gets height of the bitmap

                Matrix mat = new Matrix();
                mat.preRotate(rotate);

                //Convert to ARGB_8888
                capturedImage = Bitmap.createBitmap(capturedImage,0,0,imageWidth,imageHeight,mat,false);


            }

            capturedImage = capturedImage.copy(Bitmap.Config.ARGB_8888,true);

        } catch (Exception e) {

        }

        // Now try to recognize the characters using OCR

        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        // Here DATA_PATH is the path of the Language files present in the sdcard for example

        try {
            tessBaseAPI.init(DATA_PATH,lang);
            tessBaseAPI.setImage(capturedImage);
        } catch (Exception e) {
            String exceptionString = e.toString();
        }

        String recognizedCharacters = tessBaseAPI.getUTF8Text();
        tessBaseAPI.end();

        return recognizedCharacters;
    }

    // Caution: Always check for exceptions when using Camera.open().
    // Failing to check for exceptions if the camera is in use or
    // does not exist will cause your application to be shut down by the system.
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open(); // attempt to get a Camera instance

        } catch (Exception e) {
            Log.d("Camera main activity", e.getMessage());
            // Camera is not available (in use or does not exist)
        }
        return camera;
    }

    // To make sure the camera is released when the activity has stopped.
    // Other applications on the user's phone can use the camera afterwards
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    public void startCamera() {
        mCamera.startPreview();
    }

    public void stopCamera(){
        mCamera.stopPreview();


    }


}
